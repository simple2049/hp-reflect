package com.kvn.reflect;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by wangzhiyuan on 2018/8/14
 */
public class ConstructorAccessTest {

    @Test
    public void newInstance() {
        ConstructorAccess<FooService> access = ConstructorAccess.get(FooService.class);
        FooService fooService = access.newInstance();
        System.out.println(fooService);
    }
}